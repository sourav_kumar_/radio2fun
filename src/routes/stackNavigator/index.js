import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";

// routes
import Home from '../../screens/home'



const StackNavigation = createStackNavigator({
    Home:Home
})


export default createAppContainer(StackNavigation);


