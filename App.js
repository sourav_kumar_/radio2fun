import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import AppNavigator from './src/routes/stackNavigator'



const App = () => {
  return (
    <AppNavigator />
  );
};


export default App;
